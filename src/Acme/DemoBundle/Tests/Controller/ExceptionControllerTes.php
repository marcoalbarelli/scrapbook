<?php


namespace Acme\DemoBundle\Tests\Controller;


use Acme\DemoBundle\Controller\ExceptionController;
use Acme\DemoBundle\Exception\SomeException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExceptionControllerTest extends WebTestCase{

    /**
     * The "Exception" part of the namespace is marked as error, but tests run fine
     * @expectedException Acme\DemoBundle\Exception\SomeException
     */
    public function testExceptionThrown()
    {
        $controller = new ExceptionController();
        $controller->exceptionAction();
    }

    /**
     * Code analysis tool doesn't mark anything as error, but test fails
     * @expectedException SomeException
     */
    public function testExceptionThrown2()
    {
        $controller = new ExceptionController();
        $controller->exceptionAction();
    }

} 