<?php


namespace Acme\DemoBundle\Controller;


use Acme\DemoBundle\Exception\SomeException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ExceptionController extends Controller {


    public function exceptionAction()
    {
          throw new SomeException();
    }
} 